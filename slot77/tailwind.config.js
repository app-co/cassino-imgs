/* eslint-disable no-undef */
/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./src/**/*.{js,jsx,ts,tsx}"],
    theme: {
        extend: {
            fontFamily: {
                iceberg: ["Iceberg", "cursive"],
                iceland: ["Iceland", "cursive"],
                rubik: ["Rubik", "sans-serif"],
                poppins: ["Poppins", "sans-serif"],
                spacegrotesk: ["Space Grotesk", "sans-serif"],
                teko: ["Teko", "sans-serif"],
                inter: ["inter", "sans-serif"],
            },

            colors: {
                buttonGradientPrimary: '#d7b312',
                buttonGradientSecondary: '#d7b312',
                heroLayerGradientPrimary: 'rgba(0, 0, 0, 0)',
                heroLayerGradientSecondary: 'rgba(156, 93, 6, 0)',
                arrow: '#d7b312',
                borderArrow: '#d7b312',
                hoverBorderArrow: '#6563ef',
                asidePrimary: '#1c1e1e',
                primary: '#0D1132',
                secondary: '#1D1F53',
                intergalacticcowboy: '#222962',
                lavendersky: '#E6E3F5',
                kinglycloud: '#DFDFDF',
                flybynight: '#1c1e1e',
                gray: {
                    300: '#6B758B',
                    400: '#445970',
                },
                menu: {
                    primary: '#1c1e1e',
                },
            },
            keyframes: {
                slideUpAndFade: {
                    from: { opacity: 0, transform: "translateY(4px)" },
                    to: { opacity: 1, transform: "translateY(0)" },
                },
            },
            animation: {
                slideUpAndFade: "slideUpAndFade 100ms ease-out",
            },
        },
    },
    plugins: [require("tailwind-merge")],
};
